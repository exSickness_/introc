#include <stdio.h>
#include <string.h>

#define MAX 100

int main(void)
{
	char num[MAX];
	int number;
	register int i;
	fgets(num,MAX,stdin);
	num[strlen(num) +1] = '\0';
	number = atoi(num);
	printf("%d\n",number);
	
	for (i = 1; i <= number; i++)
	{
		if( (i % 3 == 0) && (i % 5 == 0) )
			printf("FizzBuzz\n");
		else if( i % 3 == 0)
			printf("Fizz\n");
		else if( i % 5 == 0)
			printf("Buzz\n");
		else
			printf("%d\n",i);
	}
}






//fdsagrheui







/*
The following problem is a well-known job interview filter question, often eliminating over 99.5% of candidates.

Write a program that takes an positive integer as input. The program should then print out the numbers from 1 to the input. But, for multiples of 3, print "Fizz" instead of the number, and for multiples of 5, print "Buzz". For numbers which are multiples of both 3 and 5, print "FizzBuzz".

> fizzbuzz
Enter a number to FizzBuzz: 10
1
2
Fizz
4
Buzz
Fizz
7
8
Fizz
Buzz
> fizzbuzz
Enter a number to FizzBuzz: 23
1
2
Fizz
4
Buzz
Fizz
7
8
Fizz
Buzz
11
Fizz
13
14
FizzBuzz
16
17
Fizz
19
Buzz
Fizz
22
23
> fizzbuzz
Enter a number to FizzBuzz: 6
1
2
Fizz
4
Buzz
Fizz
*/




