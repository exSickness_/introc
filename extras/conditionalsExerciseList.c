
Conditionals

Write a program that prompts the user for two numbers. The program should then print "Low/High" if the first number was less than the second number, "High/Low" if the second number was less than the first, and "Equal" if the two numbers are equal.

> hilo
Enter first number: 7.13
Enter second number: 5.71
High/Low
> hilo
Enter first number: 11
Enter second number: 11
Equal
> hilo
Enter first number: -3
Enter second number: -2
Low/High



==============================================================================================================================================================





Write a program that prompts the user for an hourly wage, and then a number of hours worked. The program should then print the wages to be paid. If more than 40 hours were worked (i.e., overtime), the employee should get 1.5 times their usual rate for every hour over 40.

> overtime
Enter hourly wage: 7.25
Enter hours worked: 35
$253.75
> overtime
Enter hourly wage: 9.32
Enter hours worked: 43
$414.74
> overtime
Enter hourly wage: 9
Enter hours worked: 40.5
$366.75

Challenge: Work correctly even if the user enters a dollar symbol in their input for the wage:

> overtime
Enter hourly wage: $7.50
Enter hours worked: 20.5
$153.75
> overtime
Enter hourly wage: 9
Enter hours worked: 40.5
$366.75




==============================================================================================================================================================






According to the United States constitution, a person is eligible to be a US Senator if they are at least 30 years old and have been a citizen for at least 9 years. They are eligible to be a US Representative if they are at least 25 years old and have been a US citizen for 7 years. To be president, a person must be at least 35 years old and be born as a US citizen (i.e., a citizen their entire life). Input a person's age and years of citizenship, and then determine their eligibility for each of these important positions.

> electable
Enter the person's age: 57
Enter the number of years of citizenship: 14
Not eligible for Presidency
Eligible for Senate
Eligible for House
> electable
Enter the person's age: 69
Enter the number of years of citizenship: 69
Eligible for Presidency
Eligible for Senate
Eligible for House
> electable
Enter the person's age: 45
Enter the number of years of citizenship: 5
Not eligible for Presidency
Not eligible for Senate
Not eligible for House





==============================================================================================================================================================







A UMBC Training Centers Instructor gives 100-point exams that are graded on the following scale:
90–100	A
80–89	B
70–79	C
60–69	D
<60	F

Write a program that inputs a student's score and uses a conditional structure to output the appropriate letter grade.

> examgrade
Enter a score: 85
B
> examgrade
Enter a score: 100
A
> examgrade
Enter a score: 26
F



==============================================================================================================================================================





A year is a "leap year" if it is divisible by 4. However, if the year is divisible by 100, it is not a leap year. Unless it's divisible by 400, in which case it's a leap year again! For example, 2000 was a leap year, but 1900 was not. Write a program that inputs a year and determines if the year is a leap year.

> intercalary
Enter a year: 2000
2000 is a leap year
> intercalary
Enter a year: 1900
1900 is not a leap year
> intercalary
Enter a year: 1984
1984 is a leap year



==============================================================================================================================================================





Write a program that prompts the user for a weight in pounds and a height in inches. It should then print the Body Mass Index (BMI), and their overall stature.

BMI is calculated by taking the weight in kilograms and dividing it by the square of the height in meters. A BMI of less than 18.5 is considered underweight, a BMI of 25 or more is considered overweight, and a BMI of 30 or more is considered obese. (1in = 0.0254m, 1kg = 2.20462lb)

> bmi
Enter weight in pounds: 146
Enter height in inches: 70
BMI is 20.948628714662586
Normal
> bmi
Enter weight in pounds: 108
Enter height in inches: 65
BMI is 17.97197749177181
Underweight
> bmi
Enter weight in pounds: 1199
Enter height in inches: 67
BMI is 187.78824426454034
Obese


