#include <stdio.h>

#define MAX 100

void changeCase(char str[]);
int strlen2(char y[]);

int main(void)
{
	char x[MAX];
	fgets(x, MAX, stdin);
	changeCase(x);
	printf("%s",x);
}

void changeCase(char str[])
{
	int count = strlen2(str);
	
	for (int i = 0; i < count -1; i++)
	{
		if ( (str[i] > 'a') && (str[i] < 'z') )
		{
			str[i] -= 32;
		}
		else if ( (str[i] > 'A') && (str[i] < 'Z') )
		{
			str[i] += 32;
		}
	}
}

int strlen2(char y[])
{
	int count = 0, x = 0;
	while ( y[x] != '\0' )
	{
		x++;
		count++;
	}
	return count;
}
