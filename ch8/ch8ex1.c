#include <stdio.h>

#define MAX 100

void reverse(char str[]);
int strlen2(char line[]);

int main(void)
{
	char x[MAX];
	
	fgets(x, MAX, stdin);
	reverse(x);
	printf("%s\n",x);
}

void reverse(char str[])
{
	int len = 0, start = 0;
	len = strlen2(str);
	int end = len -1;
	char temp;
	
	while (start < end)
	{
		temp = str[start];
		str[start] = str[end];
		str[end] = temp;
		start++;
		end--;
	}
}

int strlen2(char line[])
{
	int x = 0,count = 0;
	while( line[x] != '\0')
	{
		count++;
		x++;
	}
	return count;
}
