#include <stdio.h>

#define MAX 100

int strlen2(char line[]);
int palindrome(char str[]);

int main(void)
{
	char x[20];
	printf("Enter a string to check for palindrome: ");
	fgets(x, MAX, stdin);
	int len = strlen2(x);
	x[len-1] = '\0';
	int same = 0;
	same = palindrome(x);
	if(same)
		printf("String is not a palidrome!\n");
	else
		printf("String is a palindrome!\n");
}

int palindrome(char str[])
{
	int length = 0;
	length = strlen2(str);
	int start = 0;
	int end = length -1;
	while (start < end)
	{
		if (str[start] == str[end])
		{
			start++;
			end--;
			continue;
		}
		return 1;
	}
	return 0;
}

int strlen2(char line[])
{
	int x = 0,count = 0;
	while( line[x] != '\0')
	{
		count++;
		x++;
	}
	return count;
}
