#include <stdio.h>

int main(void)
{
	int answer1,answer2;
	double answer3,answer4;
	int a = 5, b = 10;
	answer1 = a + b;
	answer2 = a * b;
	
	double x = 25.5, y = 20;
	answer3 = x + y;
	answer4 = x * y;
	
	printf("Sum of ints        : %d\n", answer1);
	printf("Product of ints    : %d\n", answer2);

	printf("Sum of doubles     : %f\n", answer3);
	printf("Product of doubles : %f\n", answer4);
}
