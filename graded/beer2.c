#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void input(int argc, char* argv[], int* start, int* tenPlace, int* onePlace);
void printBeer(int* index1, int* index2, int* index3, int* index4);
void initLoopVars(int* index1,int* index2,int* index3,int* index4,int* tenPlace,int* onePlace,int* start);

int main(int argc, char **argv)
{
	int index1,index2,index3,index4,tenPlace,onePlace,start = 0;	

	input(argc, argv, &start, &tenPlace, &onePlace); // Input Validation function
	initLoopVars(&index1,&index2,&index3,&index4,&tenPlace,&onePlace,&start); // Setting loop values
	printBeer(&index1,&index2,&index3,&index4);	// Beer printing function
	return(0);
}


void input(int argc, char* argv[], int* start, int* tenPlace, int* onePlace)
{
	if ( argc == 1 )
	{
		*start = 99;
		*tenPlace = 9;
		*onePlace = 9;
	}

	else if ( argc == 2 )
	{
		if( (*start = (int)strtod(argv[1],'\0')) )	// if arg1 is not a number, return val is 0
		{
			if((*start > 99) || (*start < 0) )
			{
				printf("Invalid start number. Please enter a number between 1-99.\n");
				exit(1);
			}
			else
			{
				*tenPlace = argv[1][0] - 48;
				*onePlace = argv[1][1] - 48;
			}
		}
		else
		{
			printf("Invalid start number. Please enter a number between 1-99.\n");
			exit(2);
		}
	}
	else
	{
		printf("Invalid number of arguments.\n");
		exit(3);
	}
}

void initLoopVars(int* index1,int* index2,int* index3,int* index4,int* tenPlace,int* onePlace,int* start)
{
	if( *start >= 20 && *start <= 99)
	{
		*index1 = 9 - *tenPlace; // if tenPlace is 9, starts at element 0 in tens array
		*index2 = 9 - *onePlace; // if onePlace is 9, starts at element 0 in ones array.. etc
		*index3 = 0;
		*index4 = 0;
	}
	else if(*start < 20 && *start >= 10)
	{
		*index1 = 8;
		*index3 = 19 - *start;
		*index4 = 0;
	}
	else if ( *start < 10 )
	{
		*index1 = 8;
		*index3 = 10;
		*index4 = 9 - *start;
	}
	else
	{
		printf("Invalid input.\n");
		exit(21);
	}
}

void printBeer(int* index1, int* index2, int* index3, int* index4)
{
	char tens[10][50] = {"Ninety","Eighty","Seventy","Sixty","Fifty","Forty","Thirty","Twenty"};
	char teens[10][50] = {"Nineteen","Eighteen","Seventeen","Sixteen","Fifteen","Fourteen","Thirteen","Twelve","Eleven","Ten"};
	char ones[10][50] = {"Nine","Eight","Seven","Six","Five","Four","Three","Two","One",""};

	for( ; *index1 < 8; (*index1)++) // loop for beer nums 99-20
	{
		for( ; *index2 < 10; (*index2)++)
		{
			if (*index2 == 9)
			{
				printf("%s bottles of beer on the wall!\n",tens[*index1]);
				printf("%s bottles of beer!\n",tens[*index1]);
			}
			else
			{
				printf("%s-%s bottles of beer on the wall!\n",tens[*index1],ones[*index2]);
				printf("%s-%s bottles of beer!\n",tens[*index1],ones[*index2]);
			}
			printf("Take one down\nAnd pass it around\n");
			if (*index1 == 7 && *index2 == 9) // beer number 20
				printf("%s bottles of beer on the wall!\n\n",teens[0]);
			else if(*index2 == 8) // beer numbers, 91,81,71,61,51,41,31
				printf("%s bottles of beer on the wall!\n\n",tens[*index1]);
			else if(*index2 == 9) // beer numbers, 90,80,70,60,50,40,30
				printf("%s-%s bottles of beer on the wall!\n\n",tens[*index1+1],ones[0]);
			else
				printf("%s-%s bottles of beer on the wall!\n\n",tens[*index1],ones[*index2 +1]);
		}
	}
	
	for( ; *index3 < 10; (*index3)++) // loop for beer numbers 20-10
	{
		printf("%s bottles of beer on the wall!\n",teens[*index3]);
		printf("%s bottles of beer!\n",teens[*index3]);
		printf("Take one down\nAnd pass it around\n");
		if(*index3 == 9)
			printf("%s bottles of beer on the wall!\n\n",ones[0]);
		else
			printf("%s bottles of beer on the wall!\n\n",teens[*index3 +1]);
	}
	
	for( ; *index4 < 9; (*index4)++) // loop for beer numbers 9-1
	{
		if (*index4 == 8) // beer number 1
		{
			printf("%s bottle of beer on the wall!\n",ones[*index4]);
			printf("%s bottle of beer!\n",ones[*index4]);
		}
		else
		{	printf("%s bottles of beer on the wall!\n",ones[*index4]);
			printf("%s bottles of beer!\n",ones[*index4]);
		}
		printf("Take one down\nAnd pass it around\n");
		if(*index4 == 8) // beer number  1
			printf("No More bottles of beer on the wall!\n\n");
		else if(*index4 == 7) // beer number 2 ending
			printf("One bottle of beer on the wall!\n\n");
		else
			printf("%s bottles of beer on the wall!\n\n",ones[*index4 +1]);
	}
}