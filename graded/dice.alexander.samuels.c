#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define ROWS 15
#define COLS 5

int main(int argc, char **argv)
{
	int i,a;
	int rolls, sum, die1,die2;
	double histo,histo1,histo2;
	double totals[ROWS][COLS];// = {0};

	if ( argc == 1 )
	{
		rolls = 100;
		histo = 1;
	}

	else if ( argc == 3 )
	{
		if( strtod(argv[1],'\0') ) // if arg1 is not a number, return val is 0
		{
			if( strtod(argv[2],'\0') ) // if arg2 is not a number, return val is 0
			{
				rolls = strtod(argv[1], '\0');
				histo = strtod(argv[2], '\0');
			}
			else
			{
				printf("Invalid Multiplier.\n");
				exit(1);
			}
		}
	
		else
		{
			printf("Invalid Roll Number.\n");
			exit(2);
		}
	}
	else
	{
		printf("Invalid number of arguments.\n");
		exit(3);
	}

	for (i = 0; i < rolls; i++)
	{
		sum = 0;
		die1 = (rand() %6) +1;
		die2 = (rand() %6) +1;
		sum = die1 + die2;
		totals[sum][0] += 1;
	}
	
	totals[2][2] = 2.778;
	totals[3][2] = 5.556;
	totals[4][2] = 8.333;
	totals[5][2] = 11.111;
	totals[6][2] = 13.889;
	totals[7][2] = 16.667;
	totals[8][2] = 13.889;
	totals[9][2] = 11.111;
	totals[10][2] = 8.333;
	totals[11][2] = 5.556;
	totals[12][2] = 2.778;
	
	for (i = 2; i <= 12; i++)
	{
		totals[i][1] = (totals[i][0] * 100) / (double)rolls; // Calculating the actual percentage
		totals[i][3] = totals[i][1] - totals[i][2]; // Calculating the difference between expected and actual percentages
	}
	
	printf("Rolls: %5d %49s%3.0f\n",rolls,"Star Value:",histo);
	printf("%3s %7s %12s %10s %12s %11s\n","Num","Count", "Percentage","Expected","Difference","Histogram");

	for(a = 2; a < 13; a++)
	{
		histo1 = totals[a][0] / histo;
		histo2 = (int)totals[a][0] % (int)histo;
		
		printf("%3d %6.0f %10.2f %11.2f %11.2f      ",a,totals[a][0],totals[a][1],totals[a][2],totals[a][3]); // Printing out everything
		for ( i = 0; i < histo1; i++ ) // Loop that prints out histogram
			printf("*");
		printf("%d\n",(int)histo2); // Prints remainder
	}
}

