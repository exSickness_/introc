#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define ROWS 15
#define COLS 5

void input(int argc, char *argv[], double *histo, int *rolls);

int main(int argc, char *argv[])
{
	int index1,index2;
	int rolls, sum, die1,die2;
	double histo,starNum,remainder;
	double totals[ROWS][COLS];

	input(argc, argv,&histo,&rolls);

	for (index1 = 0; index1 < rolls; index1++) // Loop calculates random sum and adds 1 count to array
	{
		sum = 0;
		die1 = (rand() %6) +1;
		die2 = (rand() %6) +1;
		sum = die1 + die2;
		totals[sum][0] += 1;
	}
	
	// Inserting expected percentages into array
	totals[2][2] = 2.778;
	totals[3][2] = 5.556;
	totals[4][2] = 8.333;
	totals[5][2] = 11.111;
	totals[6][2] = 13.889;
	totals[7][2] = 16.667;
	totals[8][2] = 13.889;
	totals[9][2] = 11.111;
	totals[10][2] = 8.333;
	totals[11][2] = 5.556;
	totals[12][2] = 2.778;
	
	for (index1 = 2; index1 <= 12; index1++)
	{
		totals[index1][1] = (totals[index1][0] * 100) / (double)rolls; // Calculating the actual percentage
		totals[index1][3] = totals[index1][1] - totals[index1][2]; // Calculating the difference between expected and actual percentages
	}
	
	printf("Rolls: %5d %49s%3.0f\n",rolls,"Star Value:",histo);
	printf("%3s %7s %12s %10s %12s %11s\n","Num","Count", "Percentage","Expected","Difference","Histogram");

	for(index2 = 2; index2 < 13; index2++) // Loop prints out all data
	{
		starNum = totals[index2][0] / histo;
		remainder = (int)totals[index2][0] % (int)histo;
		
		printf("%3d %6.0f %10.2f %11.2f %11.2f      ",index2,totals[index2][0],totals[index2][1],totals[index2][2],totals[index2][3]);
		for ( index1 = 1; index1 <= starNum; index1++ ) // Loop that prints out histogram
			printf("*");
		printf("%d\n",(int)remainder); // Prints remainder
	}
}

void input(int argc, char *argv[],double *histo,int *rolls)
{
	*rolls = (double) *rolls;
	if ( argc == 1 )
	{
		*rolls = 100;
		*histo = 1;
		return;
	}

	else if ( argc == 3 )
	{
		if( strtod(argv[1],'\0') ) // if arg1 is not a number, return val is 0
		{
			if( strtod(argv[2],'\0') ) // if arg2 is not a number, return val is 0
			{
				*rolls = strtod(argv[1], '\0');
				*histo = strtod(argv[2], '\0');
				return;
			}
			else
			{
				printf("Invalid Multiplier.\n");
				exit(1);
			}
		}
	
		else
		{
			printf("Invalid Roll Number.\n");
			exit(2);
		}
	}
	else
	{
		printf("Invalid number of arguments.\n");
		exit(3);
	}
}

