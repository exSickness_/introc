#include <stdio.h>
#include <string.h>

int main(void)
{
	int x = 23, mask = 1, c = 5, j = 0;
	char out[20];
	while ( c > 0 )
	{
		if( (x & mask) == 1)
		{
			out[j++] = '1';
		}
		else
		{
			out[j++] = '0';
		}
		c--;
		x >>=1;
	}
	printf("\n");
	int i;
	for( i = 0; out[i] != '\0'; i++) // Counting contents of out[]
		;
	printf("[%s]'s length is: %d\n",out,i);
	for( int p = i; p >= 0; p--) // Printing out contents starting at end of out[]
	{
		printf("%c",out[p]);
	}
	printf("\n");
}
