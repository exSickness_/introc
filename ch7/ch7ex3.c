#include <stdio.h>

int countbits(int var);

int main(void)
{
	int count, x = 15;
	count = countbits(x);
	printf("There are %d bits set.\n",count);
}

int countbits(int var)
{
	int mask = 1, count = 0;
	while(var != 0)
	{
		if ((var & mask) == 1)
		{
			count++;
		}
		var >>= 1;
	}
	return count;
}
