#include <stdio.h>

unsigned int unset(unsigned var, int bitNum);

int main(void)
{
	unsigned int x = 0;
	x = unset(x,3);
	printf("%d is the answer\n",x);
}

unsigned int unset(unsigned var, int bitNum)
{
	int mask = 1;
	for( int i = 0; i < bitNum -1; i++ )
		mask <<= 1;
	var |= mask;
	var ^= mask;
	return var;
}
