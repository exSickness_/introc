#include <stdio.h>
#include <string.h>

long power(int base, int exponent);

int main(void)
{
	long int ans;
	int a = 3,b = 5;
	
	ans = power(a,b);
	printf("%d raised to the %d power is %ld.\n", a,b,ans);
}

long power(int base, int exp)
{
	long answer;
	answer = base;
	for(int i = 1; i < exp; i++)
	{
		answer *= base;
	}
	return(answer);
}
