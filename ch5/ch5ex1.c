#include <stdio.h>

int main(void)
{
	int c = 0;
	while( (c = getchar()) != EOF )
	{
		if (c >= '0' && c <= '9')
		{
			continue;
		}
		else
			putchar(c);
	}
}
