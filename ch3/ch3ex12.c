#include <stdio.h>

int consecInt(int x)
{
	return (x * (x + 1)) / 2;
}

int main(void)
{
	int total = 1;
	int hiNum = 1;
	int lowNum = 1;
	/*for(hiNum = 1; total <= 10000; hiNum++)
	{
		total = consecInt(hiNum);
	}*/
	while (total != 10000)
	{
		total = consecInt(hiNum);
		while (total > 10000)
		{
		
			lowNum++;
			printf("BEFORE total: %d\n",total);
			total -= consecInt(lowNum);
			printf("AFTER total: %d\n",total);
			
		}
		hiNum++;
		
		
		//printf("Cycle complete");
	}
	printf("Done.%d %d %d\n",hiNum,lowNum,total);
}

