#include <stdio.h>

int main(void)
{
	int i;

	for(i = -4; i <= 4; i++)
	{
		if(i < 0)
		{
			printf("%d is negative",i);
			if(i % 2 == 0)
			{
				printf(" and even\n");
			}
			else
			{
				printf(" and odd\n");
			}
		}
		else if(i > 0)
		{
			printf("%d is positive",i);
			if(i % 2 == 0)
			{
				printf(" and even\n");
			}
			else
			{
				printf(" and odd\n");
			}
		}
		else
		{
			printf("%d is even\n",i);
		}
	}
}
