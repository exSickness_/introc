#include <stdio.h>

int main(void)
{
	printf("Char size       : %d\n", sizeof(char));
	printf("Int size        : %d\n", sizeof(int));
	printf("Double size     : %d\n", sizeof(double));
	printf("Float size      : %d\n", sizeof(float));
	printf("Char * size     : %d\n", sizeof(char *));
	printf("Int * size      : %d\n", sizeof(int *));
	printf("Float * size    : %d\n", sizeof(float *));
	printf("Void * size     : %d\n", sizeof(void *));
	printf("Void (*)() size : %d\n", sizeof(void(*)()));
}
